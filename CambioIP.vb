﻿Imports System.Management

Imports System
Imports System.Net
Imports System.Net.NetworkInformation
Public Class CambioIP

    '<summary>
    'Pone la tarjeta de red en modo DHCP
    '</summary>
    ''<param name="ip_address">The IP Address</param>
    ''<param name="subnet_mask">The Submask IP Address</param>
    '<remarks>Requires a reference to the System.Management namespace</remarks>
    Public Shared Sub setDHCP()

        Dim objMC As New ManagementClass("Win32_NetworkAdapterConfiguration")
        Dim objMOC As ManagementObjectCollection = objMC.GetInstances()

        For Each objMO As ManagementObject In objMOC
            If CBool(objMO("IPEnabled")) Then
                Try
                    Dim newDHCPIP As ManagementBaseObject = objMO.GetMethodParameters("EnableDHCP")
                    objMO.InvokeMethod("EnableDHCP", Nothing, Nothing)
                Catch generatedExceptionName As Exception

                    MsgBox(generatedExceptionName.ToString)
                    Throw


                End Try
            End If
        Next
    End Sub


    ''' <summary>
    '''Activa una IP local y Mascara 
    ''' </summary>
    ''' <param name="ip_address">The IP Address</param>
    ''' <param name="subnet_mask">The Submask IP Address</param>
    ''' <remarks>Requires a reference to the System.Management namespace</remarks>
    Public Shared Sub setIP(ByVal ip_address As String, ByVal subnet_mask As String)

        Dim objMC As New ManagementClass("Win32_NetworkAdapterConfiguration")
        Dim objMOC As ManagementObjectCollection = objMC.GetInstances()

        For Each objMO As ManagementObject In objMOC
            If CBool(objMO("IPEnabled")) Then
                Try
                    Dim setIP As ManagementBaseObject
                    Dim newIP As ManagementBaseObject = objMO.GetMethodParameters("EnableStatic")

                    newIP("IPAddress") = New String() {ip_address}
                    newIP("SubnetMask") = New String() {subnet_mask}

                    setIP = objMO.InvokeMethod("EnableStatic", newIP, Nothing)
                Catch generatedExceptionName As Exception
                    Throw


                End Try
            End If
        Next
    End Sub
    ''' <summary>
    ''' Activa la puerta de enlace
    ''' </summary>
    ''' <param name="gateway">The Gateway IP Address</param>
    ''' <remarks>Requires a reference to the System.Management namespace</remarks>
    Public Shared Sub setGateway(ByVal gateway As String)
        Dim objMC As New ManagementClass("Win32_NetworkAdapterConfiguration")
        Dim objMOC As ManagementObjectCollection = objMC.GetInstances()

        For Each objMO As ManagementObject In objMOC
            If CBool(objMO("IPEnabled")) Then
                Try
                    Dim setGateway As ManagementBaseObject
                    Dim newGateway As ManagementBaseObject = objMO.GetMethodParameters("SetGateways")

                    newGateway("DefaultIPGateway") = New String() {gateway}
                    newGateway("GatewayCostMetric") = New Integer() {1}

                    setGateway = objMO.InvokeMethod("SetGateways", newGateway, Nothing)
                Catch generatedExceptionName As Exception
                    Throw
                End Try
            End If
        Next
    End Sub

    Public Shared Function dameIP() As String


        Dim nombrePC As String
        Dim entradasIP As Net.IPHostEntry

        nombrePC = Dns.GetHostName

        entradasIP = Dns.GetHostEntry(nombrePC)
        ''Dns.GetHostByName(nombrePC)

        Dim direccion_Ip As String = entradasIP.AddressList(0).ToString

        Return direccion_Ip

    End Function

End Class
